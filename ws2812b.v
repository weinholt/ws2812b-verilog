/* -*- mode: verilog; coding: utf-8 -*-

 Copyright © 2023 Weinholt Consulting AB
 SPDX-License-Identifier: BSD-3-Clause

 Drive the data pin of a Worldsemi WS2812B (addressable RGB LED).

 Usage:

 Be ready to output a full frame, i.e., one color for each pixel in
 the chain. Color data is sent in as GRB888, each byte is sent MSB
 first, and the data starts with the first pixel's color. Set reset=1
 when the last color bit has been sent. This will cause all pixels to
 latch their new color.

 Wait for out_done=1 before starting the next frame. You can keep
 reset=1 until you are ready to output a new frame, or you can start a
 new frame immediately after out_done goes high.

 */

`timescale 1ns/100ps

module ws2812b (clk, color, reset, out_data, out_done);
   input clk;
   input color;
   input reset;
   output out_data;
   output out_done;
   
   // WS2812B timings for clk=16 MHz
   parameter T_bit = 20;     // 1250ns
   parameter T_low = 7;      // 437ns
   parameter T_high = 13;    // 812.5ns
   parameter T_reset = 4480; // 280µs

   reg [13:0] ctr = 14'b0;
   reg        done = 1'b0;
   reg        data = 1'b0;
   assign out_done = done;
   assign out_data = data;

   always @ (posedge clk) begin
      if ((reset == 0 && ctr == T_bit-1) ||
          (reset == 1 && ctr == T_reset-1)) begin
         ctr <= 0;
         done <= 1;
      end else begin
         ctr <= ctr + 1;
         done <= 0;
      end

      if (reset == 1)
        data <= 0;
      else if (color == 0)
        data <= ctr < T_low;
      else
        data <= ctr < T_high;
   end
endmodule
