/* -*- mode: verilog; coding: utf-8 -*-

 Copyright © 2023 Weinholt Consulting AB
 SPDX-License-Identifier: BSD-3-Clause

 Demo for the WS2812B driver.

 This is a test done on an FPGA-384 board from Silicon Frog. The
 pixels will cycle between the colors in the palette. It can be made
 to cycle signficantly faster.

 */

module pixeldemo (clk, ledG, dil1);
   input clk;
   output ledG;
   output dil1;                 // connect to the LED strip

   parameter pixel_max = 8*8*2; // number of chained LEDs

   // The pixels will cycle between these colors (GRB888, reversed
   // bit order in each byte)
   parameter  color0 = 24'h000000;
   parameter  color1 = 24'h000080;
   parameter  color2 = 24'h800000;
   parameter  color3 = 24'h800080;
   parameter  color4 = 24'h008000;
   parameter  color5 = 24'h008080;
   parameter  color6 = 24'h808000;
   parameter  color7 = 24'h808080;

   reg [24:0] counter = 25'b0;
   assign ledG = counter[24];

   reg        color = 0;
   wire       ws2812b_done;
   wire       ws2812b_data;
   assign dil1 = ws2812b_data;
   ws2812b ws2812b(clk, color, ws2812b_reset, ws2812b_data, ws2812b_done);

   localparam pixel_width = 24;
   reg [7:0]  pixel_idx = 8'b0;
   reg [4:0]  color_idx = 5'b0;
   reg        ws2812b_reset = 0;
`ifndef SYNTHESIS
   reg        end_synthesis = 0;
`endif

   always @(posedge ws2812b_done) begin
`ifndef SYNTHESIS
      if (end_synthesis == 1)
        $finish;
`endif
      
      // Step between each color bit and pixel.
      if (pixel_idx == pixel_max) begin
         ws2812b_reset <= 1;
         color_idx <= 0;
         pixel_idx <= 0;
`ifndef SYNTHESIS
         end_synthesis <= 1;
`endif
      end else begin
         ws2812b_reset <= 0;
         if (color_idx == pixel_width-1) begin
            color_idx <= 0;
            pixel_idx <= pixel_idx+1;
         end else begin
            color_idx <= color_idx+1;
         end
      end

      // Read from the palette.
      case (counter[23:21]+pixel_idx[2:0])
        3'b000: color <= color0[color_idx];
        3'b001: color <= color1[color_idx];
        3'b010: color <= color2[color_idx];
        3'b011: color <= color3[color_idx];
        3'b100: color <= color4[color_idx];
        3'b101: color <= color5[color_idx];
        3'b110: color <= color6[color_idx];
        3'b111: color <= color7[color_idx];
      endcase
   end

   always @(posedge clk) begin
      counter <= counter + 1;
   end
endmodule
