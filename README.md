# WS2812B driver in Verilog

This is a simple WS2812B driver in Verilog. There is also a demo setup
for the FPGA-384 from Silicon Frog, a board with a Lattice iCE40
LP384. It uses the open Yosys toolchain for synthesis and Verilator
for simulation.

The timing is close to that specified in the WS2812B datasheet. The
reset time has been increased to 280µs to handle newer models. The
output has been checked with Verilator and with a logic analyzer in a
setup where dil1 was hooked up to two chained 8x8 pixel matrices.

More on what's needed to build for the FPGA-384:
<https://siliconfrog.co.uk/cgi/Pages/FPGA384_Prog.pl>

Please let me know if you find some problems.

## Author

Göran Weinholt <goran@weinholt.se>
