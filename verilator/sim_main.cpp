// Based on an example with this notice:
//
// This file ONLY is placed under the Creative Commons Public Domain, for
// any use, without warranty, 2017 by Wilson Snyder.
// SPDX-License-Identifier: CC0-1.0
//======================================================================

#include <verilated.h>
#include "Vtop.h"

vluint64_t main_time = 0;

// Called by $time in Verilog
double sc_time_stamp() {
    return main_time * 625.0/2.0;
}

int main(int argc, char** argv) {
    Verilated::debug(0);
    Verilated::randReset(2);
    Verilated::traceEverOn(true);
    Verilated::commandArgs(argc, argv);
    Verilated::mkdir("logs");
    Vtop* top = new Vtop;

    top->clk = 0;

    while (!Verilated::gotFinish()) {
        main_time++;
        top->clk = !top->clk;
        top->eval();

        VL_PRINTF("[%" VL_PRI64 "d] clk=%x dil1=%d\n",
                  main_time, top->clk, top->dil1);
    }

    top->final();

    //  Coverage analysis (since test passed)
#if VM_COVERAGE
    Verilated::mkdir("logs");
    VerilatedCov::write("logs/coverage.dat");
#endif

    delete top;
    top = NULL;
    exit(0);
}
